using LinearAlgebra
using Plots
const res=8
const L=640
#const L=3200
#const L=4480
const Δr=0.15625/res
const Δt=Δr^2/4
const r_dim=round(Int,L/Δr)
#const T=Δt
const T=3


function ψ0(r)
    global Δr
    x=r*Δr
    return x*π^(-1/4)*2^(5/2)*exp(-2*x^2)
end

#=
function ψ0(r)
    global Δr
    x=r*Δr
    return x*π^(-1/4)*2*exp(-x^2/2)
end
=#

function init!(Ψ)
    global r_dim
    for r in 1:r_dim
        Ψ[r]=ψ0(r)
    end
end


function V(r,A,ρ)
    global Δr
    rr=r*Δr
    if rr<500
        el=exp(rr-ρ)
        return A*el/(1+el)^2
    else
        el=exp(-rr+ρ)
        return A*el
    end
end

#=
function V(r,A,ρ)
    global Δr
    rr=r*Δr
    if rr<500
        el=exp(rr-ρ)
        return A*el/(1+el)^2
    else
        el=exp(-rr+ρ)
        return A*el
    end
end
=#

#=
function lin_pde(A,ρ)
    global Δr
    global Δt
    global r_dim
    dl=1/Δr^2/2*ones(ComplexF64,r_dim-1)
    d=zeros(ComplexF64,r_dim)
    for r in 1:r_dim
        d[r]=1im/Δt -1/Δr^2-V(r,A,ρ)/2
    end
    du=1/Δr^2/2*ones(ComplexF64,r_dim-1)
    lin_pde=Tridiagonal(dl,d,du)
    return lin_pde
end

function rhs(A,ρ)
    global Δr
    global Δt
    global r_dim
    dl=-1/Δr^2/2*ones(ComplexF64,r_dim-1)
    d=zeros(ComplexF64,r_dim)
    for r in 1:r_dim
        d[r]=1im/Δt +1/Δr^2+V(r,A,ρ)/2
    end
    du=-1/Δr^2/2*ones(ComplexF64,r_dim-1)
    rh=Tridiagonal(dl,d,du)
    return rh
end
=#

function lin_pde(A,ρ)
    global Δr
    global Δt
    global r_dim
    dl=-1im *Δt/Δr^2/2*ones(ComplexF64,r_dim-1)
    d=zeros(ComplexF64,r_dim)
    for r in 1:r_dim
        d[r]=1 +1im *Δt/Δr^2+1im *Δt*V(r,A,ρ)/2
    end
    du=-1im *Δt/Δr^2/2*ones(ComplexF64,r_dim-1)
    lin_pde=Tridiagonal(dl,d,du)
    return lin_pde
end

function rhs(A,ρ)
    global Δr
    global Δt
    global r_dim
    dl=1im *Δt/Δr^2/2*ones(ComplexF64,r_dim-1)
    d=zeros(ComplexF64,r_dim)
    for r in 1:r_dim
        d[r]=1 -1im *Δt/Δr^2-1im *Δt*V(r,A,ρ)/2
    end
    du=1im *Δt/Δr^2/2*ones(ComplexF64,r_dim-1)
    rh=Tridiagonal(dl,d,du)
    return rh
end

println("will compute until ", Int(round(T/Δt))*Δt)

function evolution(A,ρ,τ)
    global Δt
    global r_dim
    ψ=zeros(ComplexF64,r_dim)
    println("init ψ")
    init!(ψ)
    ψ0=zeros(ComplexF64,r_dim)
    ψ0=ψ
    println("init M")
    M=lin_pde(A,ρ)
    println("init rhs")
    rh=rhs(A,ρ)
    println("will need ",τ/Δt, " steps")
    Steps=round(Int,τ/Δt)
    for t in 1:Steps
        if (t-1)%round(Int,Steps/1000,RoundUp)==0
            println("we are at ", round((t-1)/Steps*100,digits=3),"%")
        end
        ψ=M\(rh*ψ)
        #display(plot(map(angle,ψ./ψ0)))
        #sleep(100)
    end
    return ψ
end

ψ=evolution(49.25,1,T)

#now format ψ so that the python code can read it
strψ=string.(ψ)

f=open("crank_nicolson_result_t$T,$L","w")

println(f, "0")

for i in 1:length(strψ)
    if i%res==0
        println(f,replace(strψ[i],"im"=>"j", " "=>""))
    end
end

close(f)


