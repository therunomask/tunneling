#2F1(a,b;c;z)=\sum_s \frac{(a)_s (b)_s}{(c)_s s!} z^s
#our special case is  b=1-a

from scipy.special import gamma, digamma, erf, lambertw
from math import pi, ceil
import numpy as np
from mpmath import mp,mpmathify,mpf,mpc,hyp2f1
from mpmath import pi as pii
from multiprocessing import Pool

def gauss_hyper_Jost(delta, c, z):
    if not 0 <= z < 1:
        print("error, z not in proper range of this implementation of hypergeometric")
        return 0
    su = 1
    re = su
    n = 1
    # use 0<z<1 and properties of F to find quickly convergent series
    if z <= .5:
        while abs(su) > 1e-17:
            su *= ((0.5+n-1)*(0.5+n-1)+delta*delta)/((c+n-1)*n)*z
            re += su
            n += 1
        return re
    else:
        # use sin(pi(c-1))/pi F(a,1-a;c;z)/gamma(c)=1/(gamma(c-a)gamma(c-1+a)
        # gamma(2-c))F(a,b;2-c;1-z)
        #    -(1-z)**(c-1)/(gamma(a)gamma(1-a) gamma(c))F(c-a,c-1+a;c;1-z)
        # <=> sin(pi(c-1))/pi
        # F(a,1-a;c;z)/gamma(c)=1/(gamma(c-a)gamma(c-1+a)gamma(2-c))F(a,b;2-c;1-z)
        #    -(1-z)**(c-1)sin(pi a )/pi  F(c-a,c-1+a;c;1-z)/gamma(c)
        su2=1
        re2=1
        while abs(su) > 1e-17:
            su *= ((0.5+n-1)*(0.5+n-1)+delta*delta)/((2-c+n-1)*n)*(1-z)
            su2 *= (c-0.5+1j*delta+n-1)*(c-0.5-1j*delta+n-1)/((c+n-1)*n)*(1-z)
            re += su
            re2 += su2
            n += 1
        re *= gamma(c)*gamma(c-1)/(gamma(c-0.5+1j*delta)*gamma(c-0.5-1j*delta))
        re2*=(1-z)**(c-1)*np.cosh(pi *delta)/np.sin(pi*(c-1))
        return re-re2


def gauss_hyper_Jost_der(delta,c,z):
    #derivativ of 2F1(1/2-i delta, 1/2 + i delta, c, z) with respect to c
    if not 0 <= z < 1:
        print("error, z not in proper range of this implementation of hypergeometric")
        return 0
    su =1 
    re =0
    insu=0
    n=1
    if z <= .5:
        while abs(su) > 1e-17:
            su *= ((0.5+n-1)*(0.5+n-1)+delta*delta)/((c+n-1)*n)*z
            insu+=1/(c+n-1)
            re -= su*insu
            n += 1
        return re
    else:
        #su is used for sum of \partial_c 2F1(a,1-a;2-c;1-z)
        #insu is used for inner sum in series of 2F1(a,1-a;2-c;1-z)
        su2=1# for \partial_c 2F!(c-a,c-1+a;c;1-z)
        re2=0
        insu2=0#inner sum for su2
        re3=1#2F1(a,1-a;2-c;1-z)
        re4=1#2F1(c-a,c-1+a;c;1-z)
        while abs(su) > 1e-17:
            su *= ((0.5+n-1)*(0.5+n-1)+delta*delta)/((2-c+n-1)*n)*(1-z)
            insu+=1/(2-c+n-1)
            re += su*insu
            re3+=su
            su2 *= (c-0.5+1j*delta+n-1)*(c-0.5-1j*delta+n-1)/((c+n-1)*n)*(1-z)
            insu2+=1/(c-0.5+1j*delta+n-1)+1/(c-0.5-1j*delta+n-1)-1/(c+n-1)
            re2+=su2*insu2
            re4+=su2
            n += 1
        f1=gamma(c)*gamma(c-1)/(gamma(c-0.5+1j*delta)*gamma(c-0.5-1j*delta))
        f2=(1-z)**(c-1)*np.cosh(pi*delta)/np.sin(pi*(c-1))
        half1=f1*(re3*(1/(c-1)+2*digamma(c-1) - digamma(c-0.5-1j*delta)-
            digamma(c - 0.5 + 1j*delta))+re) 
        half2=- f2*(re4*(np.log(1-z)-pi/np.tan(pi*(c-1)))+re2)
        return half1+half2



if abs(gauss_hyper_Jost(2.5, 2.7, 0.6)-4.461814797859006+0j) > 1e-9 or\
        abs(gauss_hyper_Jost_der(2.5, 2.7, 0.3)+0.5060979128596391) > 1e-9 or\
        abs(gauss_hyper_Jost_der(2.5, 2.4, 0.678)+4.526904095395821) > 1e-9:
        print("consistency checks failed for 2F1!")


"""next we provide an implementation of the error function and Moshinski
function  such that known cancellations in the expansion of \psi don't lead to
loss of accuracy"""

def ErfCancelCautious_red(z):
    """the error for the asymptotic expansion is estimated by z^{2 od+1}
    e^{z^2}/\Gamma(1/2+od), where the optimal order is od=|z|^2-1/2; this evaluates
    roughtly to e^{2(Re (z))^2}, which ought to be less than machine precision
    (taken to be 1e-17)"""
    #magic number tuned along angle 3/8 pi to minimize relative error
    if abs(2*np.real(z)**2+np.imag(z)**2) >22: #39/2:
        rho=abs(z)**2
        maxOrder=np.around(rho-1/2)
        alpha=maxOrder-rho +1/2
        phi=2j*np.angle(z)
        su = 1/z
        re = su
        su/=-2*z*z
        re+=su
        n = 2
        while n <= maxOrder and abs(su*z) > 1e-17:
            su *= -1*(1/2+n-1)/z/z
            re += su
            n+=1
        sign=int(np.sign(np.real(z)))
        correction= (-1)**maxOrder\
            *2*np.exp(-phi*(rho+alpha)-rho)/\
            (1+np.exp(-phi))/(np.sqrt(2*pi*rho))*\
            (1+((6*alpha**2-6*alpha+1)/12-alpha/(1+np.exp(phi))+1/(1+np.exp(phi))**2)/rho)
        return [sign, -re/np.sqrt(pi)+correction]
    else:
        su = 2*z
        re = su
        n = 1
        while abs(su) > 1e-17:
            su *= 2*z*z/(2*n+1)
            re += su
            n+=1
        return [0, re/np.sqrt(pi)]
        #return [0, np.exp(-z*z)/np.sqrt(pi)*re]


#print(f"z=1+0j, {ErfCancelCautious(1+0j)}")
#print(f"z=1+1j, {ErfCancelCautious(1+1j)}")
#print(f"z=2.5+0j, {ErfCancelCautious(2.5+0j)}")
#print(f"z=2.5+7j, {ErfCancelCautious(2.5+7j)}")
#print(f"z={(3.8+9.2j)/2}, {ErfCancelCautious((3.8+9.2j)/2)}, \n {erf((3.8+9.2j)/2)}")
#print(f"z=10+0j, {ErfCancelCautious(10+0j)}")
#print(f"z=10+3.3j, {ErfCancelCautious(10+3.3j)}")
class HighPrecision:
    precision = 16

    def __init__(self, prec=16):
        self.precision = prec
        mp.dps = prec
        self.plus = []
        self.minus = []
        self.real = []

    def ErfCancelCautiousMP(self, z):
        """ z should be in mpmath form
        the error for the asymptotic expansion is estimated by z^{2 od+1}
        e^{z^2}/\Gamma(1/2+od), where the optimal order is od=|z|^2-1/2; this evaluates
        roughtly to e^{-(Re (z))^2}, which ought to be less than the target precision
        """
        # z=mpmathify(z)
        if mp.fabs(2 * mp.re(z)**2 + mp.im(z)**2) > 15+2*self.precision:  # 39/2:increase with mp.dps
            su = 1/z
            ret = su
            n = mpmathify('1.0')
            phi = mp.arg(z)*mpc(0, 2)
            rho = mp.fabs(z)**2
            maxOrder = mp.nint(rho-mpf('0.5'))
            alpha = maxOrder-rho+mpf('0.5')
            acc=10**(-self.precision)
            while n <= maxOrder  and fabs(su) > acc:
                su *= -1*(1/2+n-1)/z/z
                ret += su
                n += 1
            si = mp.sign(mp.re(z))
            correction = (-1)**maxOrder\
                * 2 * mp.exp(-phi*(rho+alpha)-rho-z*z) /\
                (1 + mp.exp(-phi))/(mp.sqrt(2*mp.pi*rho)) *\
                (1+((6*alpha**2-6*alpha+1)/12-alpha /
                    (1 + mp.exp(phi))+1/(1 + mp.exp(phi))**2)/rho)
            #print(f"correction: {correction}")
            # correction=0
            return [si, - mp.exp(-z*z)/mp.sqrt(mp.pi)*ret+correction]
        else:
            mp.dps += 2*int(mp.fabs(2 * mp.re(z)**2 + mp.im(z)**2))
            ret = [0, mp.erf(z)]
            mp.dps -= 2*int(mp.fabs(2 * mp.re(z)**2 + mp.im(z)**2))
            return ret
            """su = 2*z
            ret = su
            n = mpmathify('1.0')
            while mp.fabs(su) > mpf(10)**(-self.precision):
                su *= 2*z*z/(2*n+1)
                ret += su
                n+=1
            return [0,  mp.exp(-z*z)/mp.sqrt(mp.pi)*ret]"""

    def ErfCancelCautious_red(self, z):
        """ z should be in mpmath form
        the error for the asymptotic expansion is estimated by z^{2 od+1}
        e^{z^2}/\Gamma(1/2+od), where the optimal order is od=|z|^2-1/2; this evaluates
        roughtly to e^{-(Re (z))^2}, which ought to be less than the target precision
        """
        # z=mpmathify(z)
        if mp.fabs(2 * mp.re(z)**2 + mp.im(z)**2) > 15+2*self.precision:  # 39/2:increase with mp.dps
            phi = mp.arg(z)*mpc(0, 2)
            rho = mp.fabs(z)**2
            maxOrder = mp.nint(rho-mpf('0.5'))
            alpha = maxOrder-rho+mpf('0.5')
            acc=10**(-self.precision)
            su = 1/z
            ret = su
            su/=-2*z*z
            ret+=su
            n = mpmathify('2.0')
            while n <= maxOrder  and mp.fabs(su*z) > acc:
                su *= -1*(1/2+n-1)/z/z
                ret += su
                n += 1
            si = mp.sign(mp.re(z))
            correction = (-1)**maxOrder\
                * 2 * mp.exp(-phi*(rho+alpha)-rho) /\
                (1 + mp.exp(-phi))/(mp.sqrt(2*mp.pi*rho)) *\
                (1+((6*alpha**2-6*alpha+1)/12-alpha /
                    (1 + mp.exp(phi))+1/(1 + mp.exp(phi))**2)/rho)
            #print(f"correction: {correction}")
            # correction=0
            return [int(si), - ret/mp.sqrt(mp.pi)+correction]
        else:
            mp.dps += 2*int(mp.fabs(2 * mp.re(z)**2 + mp.im(z)**2))
            #debug this line
            ret = [0, mp.exp(z*z)*mp.erf(z)]
            mp.dps -= 2*int(mp.fabs(2 * mp.re(z)**2 + mp.im(z)**2))
            return ret

    def Mosh3(self,k,r,al,t, cancellation=True):
        arg1=(r+2j*k*(-1j*al+1j*t))/(2*mp.sqrt((-1j*al+1j*t)))
        arg2=(r+2j*k*( 1j*al+1j*t))/(2*mp.sqrt(( 1j*al+1j*t)))
        arg3=(r+2j*k*(    al+1j*t))/(2*mp.sqrt((    al+1j*t)))
        m1=self.ErfCancelCautious_red(arg1)
        m2=self.ErfCancelCautious_red(arg2)
        m3=self.ErfCancelCautious_red(arg3)
        if cancellation and (m1[0]==m2[0]==m3[0]!=0):
            #print("relevant mosh branch")
            #check validity of formula by using more accurate numbers

            #problematic for too late times => oo * 0
            m1=mp.exp(-r*r/(-1j*al+1j*t)/4)*m1[1]
            m2=mp.exp(-r*r/( 1j*al+1j*t)/4)*m2[1]
            m3=mp.exp(-r*r/(    al+1j*t)/4)*m3[1]
        else:
            SignImk=int(mp.sign(mp.im(k)))
            if SignImk+m1[0]==0:
                m1=mp.exp(-r*r/(-1j*al+1j*t)/4)*m1[1]
            else:
                m1=(mp.sign(mp.im(k))+m1[0])*mp.exp(1j*k*r-(-1j*al+1j*t)*k*k)+mp.exp(-r*r/(-1j*al+1j*t)/4)*m1[1]
                
                #m1=np.sign(np.imag(k))+m1[0]+np.exp(-arg1*arg1)*m1[1]
                #m1*=np.exp(1j*k*r-(-1j*al+1j*t)*k*k)
            if SignImk+m2[0]==0:
                m2=mp.exp(-r*r/( 1j*al+1j*t)/4)*m2[1]
            else:
                m2=(mp.sign(mp.im(k))+m2[0])*mp.exp(1j*k*r-( 1j*al+1j*t)*k*k)+mp.exp(-r*r/( 1j*al+1j*t)/4)*m2[1]
                #m2=np.sign(np.imag(k))+m2[0]+np.exp(-arg2*arg2)*m2[1]
                #m2*=np.exp(1j*k*r-( 1j*al+1j*t)*k*k)
            if SignImk+m3[0]==0:
                m3=mp.exp(-r*r/(    al+1j*t)/4)*m3[1]
            else:
                m3=(mp.sign(mp.im(k))+m3[0])*mp.exp(1j*k*r-(    al+1j*t)*k*k)+mp.exp(-r*r/(    al+1j*t)/4)*m3[1]
                #m3=np.sign(np.imag(k))+m3[0]+np.exp(-arg3*arg3)*m3[1]
                #m3*=np.exp(1j*k*r-(    al+1j*t)*k*k)
            
        return m1+m2+m3

        def Mosh3_old(self, k, r, al, t, cancellation=True):
            m1 = self.ErfCancelCautiousMP(
                (r+2j*k*(-1j*al+1j*t))/(2*mp.sqrt((-1j*al+1j*t))))
            m2 = self.ErfCancelCautiousMP(
                (r+2j*k*( 1j*al+1j*t))/(2*mp.sqrt(( 1j*al+1j*t))))
            m3 = self.ErfCancelCautiousMP(
                (r+2j*k*(    al+1j*t))/(2*mp.sqrt((    al+1j*t))))
            if cancellation and (m1[0] == m2[0] == m3[0] != 0):
                #print("relevant mosh branch")
                m1 = mp.exp(1j*k*r-(-1j*al+1j*t)*k*k)*m1[1]
                m2 = mp.exp(1j*k*r-( 1j*al+1j*t)*k*k)*m2[1]
                m3 = mp.exp(1j*k*r-(    al+1j*t)*k*k)*m3[1]
            else:
                m1 = mp.exp(1j*k*r-(-1j*al+1j*t)*k*k)*(mp.sign(mp.im(k))+sum(m1))
                m2 = mp.exp(1j*k*r-( 1j*al+1j*t)*k*k)*(mp.sign(mp.im(k))+sum(m2))
                m3 = mp.exp(1j*k*r-(    al+1j*t)*k*k)*(mp.sign(mp.im(k))+sum(m3))

            return m1+m2+m3


    def Mosh3_corrected(self, k, r, al, t, cancellation=True):
        m1 = self.ErfCancelCautiousMP(
            (r+2j*k*(-1j*al+1j*t))/(2*mp.sqrt((-1j*al+1j*t))))
        m2 = self.ErfCancelCautiousMP(
            (r+2j*k*( 1j*al+1j*t))/(2*mp.sqrt(( 1j*al+1j*t))))
        m3 = self.ErfCancelCautiousMP(
            (r+2j*k*(    al+1j*t))/(2*mp.sqrt((    al+1j*t))))
        if cancellation and (m1[0] == m2[0] == m3[0] != 0):
            #print("relevant mosh branch")
            m1 = mp.exp(1j*k*r-(-1j*al+1j*t)*k*k)*m1[1]
            m2 = mp.exp(1j*k*r-( 1j*al+1j*t)*k*k)*m2[1]
            m3 = mp.exp(1j*k*r-(    al+1j*t)*k*k)*m3[1]
        else:
            m1 = mp.exp(1j*k*r-(-1j*al+1j*t)*k*k)*(mp.sign(mp.im(k))+sum(m1))
            m2 = mp.exp(1j*k*r-( 1j*al+1j*t)*k*k)*(mp.sign(mp.im(k))+sum(m2))
            m3 = mp.exp(1j*k*r-(    al+1j*t)*k*k)*(mp.sign(mp.im(k))+sum(m3))
        
        m1=m1+mp.exp(-r*r/(4*(-1j*al+1j
            *t)))/mp.sqrt(mp.pi)/mp.sqrt(-1j*al+1j*t)*-1j/k
        m2=m2+mp.exp(-r*r/(4*( 1j*al+1j
            *t)))/mp.sqrt(mp.pi)/mp.sqrt( 1j*al+1j*t)*-1j/k
        m2=m2+mp.exp(-r*r/(4*(    al+1j
            *t)))/mp.sqrt(mp.pi)/mp.sqrt(    al+1j*t)*-1j/k

        return m1+m2+m3


    def Mosh3Slow(self, k, r, al, t, extra1):
        mp.prec += extra1
        self.precision += extra1
        m1 = mp.erf((r+2j*k*(-1j*al+1j*t))/(2*mp.sqrt((-1j*al+1j*t))))
        m2 = mp.erf((r+2j*k*( 1j*al+1j*t))/(2*mp.sqrt(( 1j*al+1j*t))))
        m3 = mp.erf((r+2j*k*(    al+1j*t))/(2*mp.sqrt((    al+1j*t))))
        m1 = mp.exp(1j*k*r-(-1j*al+1j*t)*k*k)*(mp.sign(mp.im(k))+m1)
        m2 = mp.exp(1j*k*r-( 1j*al+1j*t)*k*k)*(mp.sign(mp.im(k))+m2)
        m3 = mp.exp(1j*k*r-(    al+1j*t)*k*k)*(mp.sign(mp.im(k))+m3)
        m = m1+m2+m3
        if (mp.fabs(m))/(mp.fabs(m1)+mp.fabs(m2)+mp.fabs(m3)) < 1e-5:
            print(f"danger! loss of accuracy when adding error functions!")
        mp.prec -= extra1
        self.precision -= extra1
        return m

    def gauss_trio(self, k, alpha):
        return mp.exp(1j*alpha*k*k)+mp.exp(-1j*alpha*k*k)+mp.exp(-alpha*k*k)

    def gauss_trio_prime(self, k, alpha):
        return 2j * alpha * k * mp.exp(1j * alpha * k * k) -\
            2j*alpha*k*mp.exp(-1j * alpha * k * k) - \
            2 * alpha * k * mp.exp(-alpha * k * k)

    def newton_gauss(self, x, alpha):
        f = self.gauss_trio(x, alpha)
        counter = 1
        for count in range(10):
            x = x-f/self.gauss_trio_prime(x, alpha)
            f = self.gauss_trio(x, alpha)
#        if abs(f) > 1e-18:
#            print(f"f={abs(f)} is really really hard to improve upon!!")
#            print(f"f'={abs(self.gauss_trio_prime(x,alpha))}")
        return x

    def zeros(self, rad, al):
        lastroot = 0
        count = 1
        plus = []
        minus = []
        real = []
        while mp.fabs(lastroot) < rad:
            plus.insert(0, self.newton_gauss(-mp.exp(3j*mp.pi/8) *
                                             mp.sqrt(mp.pi*(2*count-1)
                                                     / mp.sqrt(2)/al), al))
            plus.append(self.newton_gauss(mp.exp(3j*mp.pi/8) *
                                          mp.sqrt(mp.pi*(2*count-1)
                                                  / mp.sqrt(2)/al), al))
            minus.insert(0, self.newton_gauss(-mp.exp(-3j*mp.pi/8) *
                                              mp.sqrt(mp.pi*(2*count-1)
                                                      / mp.sqrt(2)/al), al))
            minus.append(self.newton_gauss(mp.exp(-3j*mp.pi/8) *
                                           mp.sqrt(mp.pi*(2*count-1)
                                                   / mp.sqrt(2)/al), al))
            real.insert(0, self.newton_gauss(- mp.sqrt(mp.pi*(2*count-1)
                                                       / 2/al), al))
            real.append(self.newton_gauss(mp.sqrt(mp.pi*(2*count-1)
                                                  / 2/al), al))
            count += 1
            lastroot = plus[-1]
        self.plus = plus
        self.minus = minus
        self.real = real

    def HypLateRoot(self,n, de, rho):
        """this function returns approximate roots k_n of Hypergeometric2F1(1/2+1j*d, 1/2-1j
        *d,1+2j* k,z) as a function of k. The approximation
        seems to be of order 1/n, where n enumerates the roots in the second quadrant of
        the complex plane starting close to the imaginary axis. A proof of the
        formula is still to be found"""
        return -(2*pii*(n-mpf(1/4)) + 1j*mp.log(mp.exp(pii*de)+mp.exp(-pii*de)))\
            / (2*rho + 2j*pii)

    def HypRoots(self,rad,de,rho):
        z=1/(1+mp.exp(-rho))
        roots=[self.NewtonHypder(de,self.HypLateRoot(10,de,rho),z),
                self.NewtonHypder(de,self.HypLateRoot(11,de,rho),z)]
        ind=-1
        for c in range(9,0,-1):
            roots.insert(0,self.NewtonHypder(de,2*roots[0]-roots[1],z))
            if ind==-1 and abs(roots[0])<rad:
                ind=c+1
        if ind==-1:
            if abs(roots[-2])<rad:
                ind=11
        c=len(roots)
        while ind==-1:
            c+=1
            roots.append(self.NewtonHypder(de,2*roots[-1]-roots[-2],z))
            if ind==-1 and abs(roots[-1])>rad:
                ind=c
        roots=roots[:ind]
        roots2=[-k.conjugate() for k in roots]
        return roots[::-1]+roots2


    def wr(self,de,k,z):
        if mp.fabs(z)<mpmathify("1e-1000000000000000000"):
            return mpmathify(1)
        #print(f"{1/2-1j*complex(de)}, complex conjugated.., {1+2j*k}, z={z}")
        #print(f""" z<1e-16000000000000000000:
        #        {mp.fabs(z)<1e-16000000000000000000}""")
        return hyp2f1(mpc(1)/2-1j*mpc(de),mpc(1)/2+1j*mpc(de),1+2j*k,z)

    def hypder(self,de,k,z):
        h=hyp2f1
        #adjust accuracy of calculation
        #adjustment based on assumption that values of f are of order 1
        old=mp.dps
        mp.dps=mp.ceil((5*mp.log(5)/4-mp.log(4))/mp.log(10)+5*mp.dps/4)
        d=mp.exp(-mp.dps/5*mp.log(10)-mp.log(4)/5)
        r=-self.wr(de,k+2*d,z)/12 + 2*self.wr(de,k+d,z)/3 \
            -self.wr(de,k-d,z)*2/3 + self.wr(de,k-2*d,z)/12 
        
        r/=d
        mp.dps=old
        return r
        
    def NewtonHypder(self,de,k,z):
        c=0
        f=self.wr(de,k,z)
        while c<10:
            k=k-f/self.hypder(de,k,z)
            f=self.wr(de,k,z)
            c+=1
        if mp.fabs(f)>1e-14:
            print(f"hard to improve on |f|={mp.fabs(f)}")
        return k


    def jost(self,de,k,r,rho):
        z=1/(mpc(1)+mp.exp(r-rho))
        return mp.exp(-1j*k*r)*self.wr(de,k,z)

    def djost_dk_gen_r(self,de,k,r,rho):
        z=1/(mpc(1)+mp.exp(r-rho))
        r1=-1j*r*mp.exp(-1j*r*k)*self.wr(de,k,z)
        r2=mp.exp(-1j*k*r)*self.hypder(de,k,z)
        return r1+r2

    def djost_dk(self,de,k,rho):
        z=1/(mpc(1)+mp.exp(-rho))
        ret=self.hypder(de,k,z)
        return ret

def Mosh(k, r, bet):
    return np.exp(1j*k*r-bet*k*k) *\
        (np.sign(np.imag(k))+erf((r+2j*k*bet)/(2*np.sqrt(bet))))

def Mosh3(k,r,al,t, cancellation=True):
    arg1=(r+2j*k*(-1j*al+1j*t))/(2*np.sqrt((-1j*al+1j*t)))
    arg2=(r+2j*k*( 1j*al+1j*t))/(2*np.sqrt(( 1j*al+1j*t)))
    arg3=(r+2j*k*(    al+1j*t))/(2*np.sqrt((    al+1j*t)))
    m1=ErfCancelCautious_red(arg1)
    m2=ErfCancelCautious_red(arg2)
    m3=ErfCancelCautious_red(arg3)
    if cancellation and (m1[0]==m2[0]==m3[0]!=0):
        #print("relevant mosh branch")
        #check validity of formula by using more accurate numbers

        #problematic for too late times => oo * 0
        m1=np.exp(-r*r/(-1j*al+1j*t)/4)*m1[1]
        m2=np.exp(-r*r/( 1j*al+1j*t)/4)*m2[1]
        m3=np.exp(-r*r/(    al+1j*t)/4)*m3[1]
    else:
        SignImk=int(np.sign(np.imag(k)))
        if SignImk+m1[0]==0:
            m1=np.exp(-r*r/(-1j*al+1j*t)/4)*m1[1]
        else:
            m1=(np.sign(np.imag(k))+m1[0])*np.exp(1j*k*r-(-1j*al+1j*t)*k*k)+np.exp(-r*r/(-1j*al+1j*t)/4)*m1[1]
            
            #m1=np.sign(np.imag(k))+m1[0]+np.exp(-arg1*arg1)*m1[1]
            #m1*=np.exp(1j*k*r-(-1j*al+1j*t)*k*k)
        if SignImk+m2[0]==0:
            m2=np.exp(-r*r/( 1j*al+1j*t)/4)*m2[1]
        else:
            m2=(np.sign(np.imag(k))+m2[0])*np.exp(1j*k*r-( 1j*al+1j*t)*k*k)+np.exp(-r*r/( 1j*al+1j*t)/4)*m2[1]
            #m2=np.sign(np.imag(k))+m2[0]+np.exp(-arg2*arg2)*m2[1]
            #m2*=np.exp(1j*k*r-( 1j*al+1j*t)*k*k)
        if SignImk+m3[0]==0:
            m3=np.exp(-r*r/(    al+1j*t)/4)*m3[1]
        else:
            m3=(np.sign(np.imag(k))+m3[0])*np.exp(1j*k*r-(    al+1j*t)*k*k)+np.exp(-r*r/(    al+1j*t)/4)*m3[1]
            #m3=np.sign(np.imag(k))+m3[0]+np.exp(-arg3*arg3)*m3[1]
            #m3*=np.exp(1j*k*r-(    al+1j*t)*k*k)
        
    return m1+m2+m3

def Mosh3_corrected(k,r,al,t, cancellation=True):
    arg1=(r+2j*k*(-1j*al+1j*t))/(2*np.sqrt((-1j*al+1j*t)))
    arg2=(r+2j*k*( 1j*al+1j*t))/(2*np.sqrt(( 1j*al+1j*t)))
    arg3=(r+2j*k*(    al+1j*t))/(2*np.sqrt((    al+1j*t)))
    m1=ErfCancelCautious_red(arg1)
    m2=ErfCancelCautious_red(arg2)
    m3=ErfCancelCautious_red(arg3)
    if cancellation and (m1[0]==m2[0]==m3[0]!=0):

        #problematic for too late times => oo * 0
        m1=np.exp(-r*r/(-1j*al+1j*t)/4)*m1[1]
        m2=np.exp(-r*r/( 1j*al+1j*t)/4)*m2[1]
        m3=np.exp(-r*r/(    al+1j*t)/4)*m3[1]
    else:
        SignImk=int(np.sign(np.imag(k)))
        if SignImk+m1[0]==0:
            m1=np.exp(-r*r/(-1j*al+1j*t)/4)*m1[1]
        else:
            m1=(np.sign(np.imag(k))+m1[0])*np.exp(1j*k*r-(-1j*al+1j*t)*k*k)+np.exp(-r*r/(-1j*al+1j*t)/4)*m1[1]
            
        if SignImk+m2[0]==0:
            m2=np.exp(-r*r/( 1j*al+1j*t)/4)*m2[1]
        else:
            m2=(np.sign(np.imag(k))+m2[0])*np.exp(1j*k*r-( 1j*al+1j*t)*k*k)+np.exp(-r*r/( 1j*al+1j*t)/4)*m2[1]
        if SignImk+m3[0]==0:
            m3=np.exp(-r*r/(    al+1j*t)/4)*m3[1]
        else:
            m3=(np.sign(np.imag(k))+m3[0])*np.exp(1j*k*r-(    al+1j*t)*k*k)+np.exp(-r*r/(    al+1j*t)/4)*m3[1]
        
    m1=m1+np.exp(-r*r/(4*(-1j*al+1j
        *t)))/np.sqrt(np.pi)/np.sqrt(-1j*al+1j*t)*-1j/k
    m2=m2+np.exp(-r*r/(4*( 1j*al+1j
        *t)))/np.sqrt(np.pi)/np.sqrt( 1j*al+1j*t)*-1j/k
    m2=m2+np.exp(-r*r/(4*(    al+1j
        *t)))/np.sqrt(np.pi)/np.sqrt(    al+1j*t)*-1j/k
    return m1+m2+m3



def gauss_trio(k,alpha):
    return np.exp(1j*alpha*k*k)+np.exp(-1j*alpha*k*k)+np.exp(-alpha*k*k)

def gauss_trio_prime(k, alpha):
    return 2j * alpha * k * np.exp(1j * alpha * k * k) -\
        2j*alpha*k*np.exp(-1j * alpha * k * k) - \
        2 * alpha * k * np.exp(-alpha * k * k)

def newton_gauss(x,alpha):
    f=gauss_trio(x,alpha)
    counter=1
    while abs(f)>1e-11:
        x=x-f/gauss_trio_prime(x,alpha)
        f=gauss_trio(x,alpha)
        counter+=1
        if counter>4:
            print(f"f={abs(f)} is hard to improve upon")
            print(f"f'={abs(gauss_trio_prime(x,alpha))}")
            break
    return x

def gauss_trio_zeros(n, al):
    plus = [newton_gauss(-np.exp(3j*pi/8) * np.sqrt(pi*(2*k-1) \
            / np.sqrt(2)/al), al) for k in range(n, 0, -1)] \
            + [newton_gauss(np.exp(3j*pi/8)
            * np.sqrt(pi*(2*k-1) / np.sqrt(2)/al), al) for k in range(1, n+1)]
    minus = [newton_gauss(-np.exp(-3j*pi/8) * np.sqrt(pi*(2*k-1) \
            / np.sqrt(2)/al), al) for k in range(n, 0, -1)] \
            + [newton_gauss(np.exp(-3j*pi/8)
            * np.sqrt(pi*(2*k-1) / np.sqrt(2)/al), al) for k in range(1, n+1)]
    real = [newton_gauss(- np.sqrt(pi*(2*k-1) \
            / 2/al), al) for k in range(n, 0, -1)] \
            + [newton_gauss(np.sqrt(pi*(2*k-1) / 2/al), al) for k in range(1, n+1)]
    return plus,minus,real



def psi0(r,rho):
    return r*2*(2/rho)**(3/2)/mp.pi**(1/4) *mp.exp(-2*(r/rho)**2)

def tran1(arg):
    ob,k,al,de,rho=arg
    return k * ob.Eigentransform(k) /\
        ob.prec.gauss_trio_prime(k,al) / ob.prec.jost(de,-k,0,rho)

def tran2(arg):
    ob,k,al,de,rho=arg
    return k*ob.Eigentransform(k)/\
        ob.prec.gauss_trio(k,al)/(-ob.prec.djost_dk(de,-k,rho))

class psi:
    def __init__(self,prec,RootRad,al,de,rho):
        self.de=mpmathify(de)
        self.rho=mpmathify(rho)
        self.al=mpmathify(al)
        self.prec=HighPrecision(prec)
        self.prec.zeros(RootRad,al)
        self.GaussZerosplus=self.prec.plus
        self.GaussZerosminus=self.prec.minus
        self.GaussZerosreal=self.prec.real
        """function yields zeros of jost(de,k,0,rho), need zeros of
        jost(de,-k,0,rho)"""
        self.HypZeros=[-k for k in self.prec.HypRoots(RootRad,self.de,self.rho)]

        #preparations for psi_infty and therefore t_alg
        def f(x):
           return self.prec.jost(self.de,0,x,self.rho)
        def df(x):
           return self.prec.djost_dk_gen_r(self.de,0,x,self.rho)
        def integrand(x):
           return psi0(x,self.rho)*(df(0)*f(x)-f(0)*df(x))/1j
        C0, er=mp.quad(integrand,[0,mp.inf],error=True)
        self.C0=C0
        print(f"C0: {C0}, error: {er}")
        self.k0=self.HypZeros[len(self.HypZeros)//2-1]
        self.Ck0=self.Eigentransform(self.k0)

        """preparation of residues up to terms depending on r"""
        #print("plus:")
        with Pool(8) as p:
            le=len(self.GaussZerosplus)
            self.plusTrans=p.map(tran1,[[self,k,al,de,rho] for k
                in self.GaussZerosplus])
        #self.plusTrans=[  for k in
        #        self.GaussZerosplus]
        #exploit symmetry to save on computation time
        self.minusTrans=[c.conjugate() for c in self.plusTrans[::-1]]
        #print("minus:")
        """self.minusTrans=[ k*self.Eigentransform(k) /\
                self.prec.gauss_trio_prime(k,al) / self.prec.jost(de,-k,0,rho)for k in
                self.GaussZerosminus]"""
        #print([abs(k) for k in self.minusTrans])
        print("real:")
        with Pool(8) as p:
            self.realTrans=p.map(tran1,[[self,k,al,de,rho] for k
                in self.GaussZerosreal])
        #self.realTrans=[ k*self.Eigentransform(k)/\
        #        self.prec.gauss_trio_prime(k,al) / self.prec.jost(de,-k,0,rho) for k
        #        in self.GaussZerosreal]
        #print([abs(k) for k in self.realTrans])
        print("hyp:")
        with Pool(8) as p:
            self.hypTrans=p.map(tran2,[[self,k,al,de,rho] for k in self.HypZeros])
        #self.hypTrans=[ k*self.Eigentransform(k)/\
        #        self.prec.gauss_trio(k,al)/(-self.prec.djost_dk(de,-k,rho)) for k
        #        in self.HypZeros]
        print("end init:")
        #print([abs(k) for k in self.hypTrans])
        
    def psi_infty(self,r):
        #global phases are ignored, this is only for evaluation of t_alg
        f0= self.prec.jost(self.de,0,0,self.rho)
        fr= self.prec.jost(self.de,0,r,self.rho)
        df0= self.prec.djost_dk_gen_r(self.de,0,0,self.rho)
        dfr= self.prec.djost_dk_gen_r(self.de,0,r,self.rho)
        term= -dfr/f0+fr*df0/(f0)**2
        return -self.C0/(2*mp.sqrt(1j*mp.pi))  * term
        
        
    def talg(self,r):
        ImE=np.imag(self.k0**2)
        ab=self.psi_infty(r)*self.prec.djost_dk(self.de,-self.k0,self.rho)/\
                self.k0/self.Ck0/self.prec.jost(self.de,-self.k0,r,self.rho)
        re=mp.lambertw(ImE*2**mpf("1/3")/3*mp.fabs(ab)**mpf("2/3"),-1)*3/2/ImE
        return re


    def Eigentransform(self,k):
        def ff(l,r):
             return self.prec.jost(self.de,l,0,self.rho)*\
                self.prec.jost(self.de,-l,r,self.rho)
        def f(r):
           return (ff(k,r)-ff(-k,r))/2j/k*psi0(r,self.rho)
        val,er=mp.quad(f,[0,mp.inf],error=True)
        """
        if er>1e-30:
            val,er=mp.quad(f,[0,mp.inf],error=True,maxdegree=10)
        if er>1e-30:
            print(f"integration error too big, {er} for value {abs(val)}")
        """
        return val
    

    def eval(self,t,r):
        t=mpmathify(t)
        r=mpmathify(r)
        z=1/(1+mp.exp(r-self.rho))
        val=sum([self.prec.Mosh3(self.GaussZerosplus[k],r,self.al,t) *\
            self.plusTrans[k] * self.prec.wr(self.de,-self.GaussZerosplus[k], z)
            for k in range(len(self.GaussZerosplus))])
        val+=sum([self.prec.Mosh3(self.GaussZerosminus[k],r,self.al,t) *\
            self.minusTrans[k] * self.prec.wr(self.de,-self.GaussZerosminus[k], z)
            for k in range(len(self.GaussZerosminus))])
        val+=sum([self.prec.Mosh3(self.GaussZerosreal[k],r,self.al,t) *\
            self.realTrans[k] * self.prec.wr(self.de,-self.GaussZerosreal[k], z)
            for k in range(len(self.GaussZerosreal))])
        val+=sum([self.prec.Mosh3(self.HypZeros[k],r,self.al,t,cancellation=False) *\
            self.hypTrans[k] * self.prec.wr(self.de,-self.HypZeros[k], z)
            for k in range(len(self.HypZeros))])
        return val

    def aSum(self,r,n):
        z=1/(1+mp.exp(r-self.rho))
        val=sum([ self.plusTrans[k] *
            self.prec.wr(self.de,-self.GaussZerosplus[k],
                z)/self.GaussZerosplus[k]**n
            for k in range(len(self.GaussZerosplus))])
        val+=sum([ self.minusTrans[k] *
            self.prec.wr(self.de,-self.GaussZerosminus[k],
                z)/self.GaussZerosminus[k]**n
            for k in range(len(self.GaussZerosminus))])
        val+=sum([ self.realTrans[k] *
            self.prec.wr(self.de,-self.GaussZerosreal[k],
                z)/self.GaussZerosreal[k]**n
            for k in range(len(self.GaussZerosreal))])
        val+=sum([ self.hypTrans[k] * self.prec.wr(self.de,-self.HypZeros[k],
            z)/self.HypZeros[k]**n
            for k in range(len(self.HypZeros))])
        return val


    def eval_corrected(self,t,r):
        z=1/(1+mp.exp(r-self.rho))
        val=sum([(self.prec.Mosh3_corrected(self.GaussZerosplus[k],r,self.al,t)) *\
            self.plusTrans[k] * self.prec.wr(self.de,-self.GaussZerosplus[k], z)
            for k in range(len(self.GaussZerosplus))])
        val+=sum([self.prec.Mosh3_corrected(self.GaussZerosminus[k],r,self.al,t) *\
            self.minusTrans[k] * self.prec.wr(self.de,-self.GaussZerosminus[k], z)
            for k in range(len(self.GaussZerosminus))])
        val+=sum([self.prec.Mosh3_corrected(self.GaussZerosreal[k],r,self.al,t) *\
            self.realTrans[k] * self.prec.wr(self.de,-self.GaussZerosreal[k], z)
            for k in range(len(self.GaussZerosreal))])
        val+=sum([self.prec.Mosh3_corrected(self.HypZeros[k],r,self.al,t,cancellation=False) *\
            self.hypTrans[k] * self.prec.wr(self.de,-self.HypZeros[k], z)
            for k in range(len(self.HypZeros))])
        return val


    def partial_eval(self,t,r,rad):
        print("begin partial eval")
        z=1/(1+mp.exp(r-self.rho))
        val=mpc(0)
        for k in range(len(self.GaussZerosplus)):
            if abs(self.GaussZerosplus[k])<rad:
                val+=self.prec.Mosh3(self.GaussZerosplus[k],r,self.al,t) *\
                                self.plusTrans[k] *\
                                self.prec.wr(self.de,-self.GaussZerosplus[k], z)
                val+=self.prec.Mosh3(self.GaussZerosminus[k],r,self.al,t) *\
                    self.minusTrans[k] *\
                    self.prec.wr(self.de,-self.GaussZerosminus[k], z)

            if abs(self.GaussZerosreal[k])<rad:
                val+=self.prec.Mosh3(self.GaussZerosreal[k],r,self.al,t) *\
                                    self.realTrans[k] *\
                                    self.prec.wr(self.de,-self.GaussZerosreal[k], z)
        for k in range(len(self.HypZeros)):
            if abs(self.HypZeros[k])<rad:
                val+=self.prec.Mosh3(self.HypZeros[k],r,self.al,t,cancellation=False) *\
                    self.hypTrans[k] * self.prec.wr(self.de,-self.HypZeros[k], z)

        print("end partial eval")
        return val
#print(gauss_trio(np.exp(-3j*pi/8)*np.sqrt(pi*7/np.sqrt(2)/4),4))
#print(gauss_trio(np.exp(3j*pi/8)*np.sqrt(pi*7/np.sqrt(2)/4),4))
#print(gauss_trio(np.sqrt(pi*7/2/4),4))


class psi_standard:
    def __init__(self,prec,RootRad,al,de,rho):
        self.de=de
        self.rho=rho
        self.al=al
        self.prec=HighPrecision(prec)
        self.prec.zeros(RootRad,mpf(al))
        self.GaussZerosplus=[complex(x) for x in self.prec.plus]
        self.GaussZerosminus=[complex(x) for x in self.prec.minus]
        self.GaussZerosreal=[complex(x) for x in self.prec.real]
        """function yields zeros of jost(de,k,0,rho), need zeros of
        jost(de,-k,0,rho)"""
        self.HypZeros=[complex(-k) for k in self.prec.HypRoots(RootRad,de,rho)]

        #preparations for psi_infty and therefore t_alg
        f=lambda x: self.prec.jost(self.de,0,x,self.rho)
        df=lambda x: self.prec.djost_dk_gen_r(self.de,0,x,self.rho)
        integrand=lambda x: psi0(x,self.rho)*(df(0)*f(x)-f(0)*df(x))/1j
        C0, er=mp.quad(integrand,[0,mp.inf],error=True)
        self.C0=C0
        print(f"C0: {C0}, error: {er}")
        self.k0=self.HypZeros[len(self.HypZeros)//2-1]
        self.Ck0=self.Eigentransform(self.k0)

        """preparation of residues up to terms depending on r"""
        with Pool(8) as p:
            le=len(self.GaussZerosplus)
            dummy=p.map(tran1,[[self,k,al,de,rho] for k
                in self.prec.plus])
            self.plusTrans=[complex(x) for x in dummy]
        #exploit symmetry to save on computation time
        self.minusTrans=[c.conjugate() for c in self.plusTrans[::-1]]
        print("real:")
        with Pool(8) as p:
            dummy=p.map(tran1,[[self,k,al,de,rho] for k
                in self.prec.real])
            self.realTrans=[complex(x) for x in dummy]
        print("hyp:")
        with Pool(8) as p:
            dummy=p.map(tran2,[[self,k,al,de,rho] for k in self.HypZeros])
            self.hypTrans=[complex(x) for x in dummy]


        print("end init:")

    def psi_infty(self,r):
        #global phases are ignored, this is only for evaluation of t_alg
        f=lambda x: self.prec.jost(self.de,0,x,self.rho)
        df=lambda x: self.prec.djost_dk_gen_r(self.de,0,x,self.rho)
        term= -df(r)/f(0)+f(r)*df(0)/(f(0))**2
        return self.C0/(2*mp.sqrt(1j*mp.pi))  * term
        
        
    def talg(self,r):
        ImE=np.imag(self.k0**2)
        ab=self.psi_infty(r)*self.prec.djost_dk(self.de,-self.k0,self.rho)/\
                self.k0/self.Ck0/self.prec.jost(self.de,-self.k0,r,self.rho)
        re=lambertw(ImE*2**(1/3)/3*np.abs(complex(ab))**(2/3),-1)*3/2/ImE
        return re

    def Eigentransform(self,k):
        ff=lambda l,r: self.prec.jost(self.de,l,0,self.rho)*\
                self.prec.jost(self.de,-l,r,self.rho)
        f=lambda r: (ff(k,r)-ff(-k,r))/2j/k*psi0(r,self.rho)
        val,er=mp.quad(f,[0,mp.inf],error=True)
        return val
    

    def eval(self,t,r):
        z=1/(1+np.exp(r-self.rho))
        val=sum([Mosh3(self.GaussZerosplus[k],r,self.al,t) *\
            self.plusTrans[k] *complex(
                self.prec.wr(self.de,-self.GaussZerosplus[k], z))
            for k in range(len(self.GaussZerosplus))])
        val+=sum([Mosh3(self.GaussZerosminus[k],r,self.al,t) *\
            self.minusTrans[k] *
            complex(self.prec.wr(self.de,-self.GaussZerosminus[k], z))
            for k in range(len(self.GaussZerosminus))])
        val+=sum([Mosh3(self.GaussZerosreal[k],r,self.al,t) *\
            self.realTrans[k] *
            complex(self.prec.wr(self.de,-self.GaussZerosreal[k], z))
            for k in range(len(self.GaussZerosreal))])
        val+=sum([Mosh3(self.HypZeros[k],r,self.al,t,cancellation=False) *\
            self.hypTrans[k] * complex(self.prec.wr(self.de,-self.HypZeros[k], z))
            for k in range(len(self.HypZeros))])
        return val


    def eval_corrected(self,t,r):
        z=1/(1+mp.exp(r-self.rho))
        val=sum([(Mosh3_corrected(self.GaussZerosplus[k],r,self.al,t)) *\
            self.plusTrans[k] * self.prec.wr(self.de,-self.GaussZerosplus[k], z)
            for k in range(len(self.GaussZerosplus))])
        val+=sum([Mosh3_corrected(self.GaussZerosminus[k],r,self.al,t) *\
            self.minusTrans[k] * self.prec.wr(self.de,-self.GaussZerosminus[k], z)
            for k in range(len(self.GaussZerosminus))])
        val+=sum([Mosh3_corrected(self.GaussZerosreal[k],r,self.al,t) *\
            self.realTrans[k] * self.prec.wr(self.de,-self.GaussZerosreal[k], z)
            for k in range(len(self.GaussZerosreal))])
        val+=sum([Mosh3_corrected(self.HypZeros[k],r,self.al,t,cancellation=False) *\
            self.hypTrans[k] * self.prec.wr(self.de,-self.HypZeros[k], z)
            for k in range(len(self.HypZeros))])
        return val


    def partial_eval(self,t,r,rad):
        print("begin partial eval")
        z=1/(1+mp.exp(r-self.rho))
        val=mpc(0)
        for k in range(len(self.GaussZerosplus)):
            if abs(self.GaussZerosplus[k])<rad:
                val+=self.prec.Mosh3(self.GaussZerosplus[k],r,self.al,t) *\
                                self.plusTrans[k] *\
                                self.prec.wr(self.de,-self.GaussZerosplus[k], z)
                val+=self.prec.Mosh3(self.GaussZerosminus[k],r,self.al,t) *\
                    self.minusTrans[k] *\
                    self.prec.wr(self.de,-self.GaussZerosminus[k], z)

            if abs(self.GaussZerosreal[k])<rad:
                val+=self.prec.Mosh3(self.GaussZerosreal[k],r,self.al,t) *\
                                    self.realTrans[k] *\
                                    self.prec.wr(self.de,-self.GaussZerosreal[k], z)
        for k in range(len(self.HypZeros)):
            if abs(self.HypZeros[k])<rad:
                val+=self.prec.Mosh3(self.HypZeros[k],r,self.al,t,cancellation=False) *\
                    self.hypTrans[k] * self.prec.wr(self.de,-self.HypZeros[k], z)

        print("end partial eval")
        return val



if __name__=="__main__":
    import numpy as np
    import matplotlib
    matplotlib.rcParams['text.usetex'] = True
    import matplotlib.pyplot as plt
    import time

    #acc= HighPrecision(64)
    #acc.zeros(4.6,mpf(4))
    #print(acc.HypLateRoot(10,0.25,1))
    #print(abs(acc.wr(0.25,-acc.HypLateRoot(10,0.25,1),1/(1+mp.exp(-1)))))
    #print(acc.HypRoots(4.6,0.25,1))


    print(f"""psi0(1)={psi0(1.0,1)}""")
    #ps=psi(64,10,mpf(4),1/4,1)
    #ps=psi(16,19,mpf(1.5),1/4,1)
    start=time.perf_counter()
    #reduce number of roots for quicker debugging
    rootrad=45
    delta=8
    digits=16
    ps=psi_standard(digits,rootrad,1.25,delta,1)
    #ps=psi(16,19,mpf(1.25),1/4,1)
    print(f"""psi(0,0.75)={ps.eval_corrected(0,0.75)},
            \npsi0(0.75)={psi0(0.75,1)}""")

    x = np.linspace(0.02, 2.5, 120)
    y = [float(abs(ps.eval(0,xx
        )-psi0(xx,1)))  for xx in x]

    fig, ax = plt.subplots(2,1)

    # Using set_dashes() to modify dashing of an existing line
    ax[0].plot(x, y )
    ax[0].set_ylabel(r'error of $\psi0$')
    y = [float(psi0(mpf(xx),mpf(1)))  for xx in x]
    ax[1].plot(x, y )
    ax[1].set_ylabel(r'$\psi0$')
    # Using plot(..., dashes=...) to set the dashing when creating a line
    #line2, = ax.plot(x, y - 0.2, dashes=[6, 2], label='Using the dashes parameter')

    fig.tight_layout()
    #plt.show()
    plt.savefig(f"initial_wavefunction_for_de={delta}_rootrad={rootrad}_digits={digits}.png")
    print(f"end: {time.perf_counter()-start}")
    """
    accurate = HighPrecision(62)
    accurate.zeros(20,mpf(4))
    #print([(mp.nstr(abs(accurate.Mosh3(k,1,4,3)),4),mp.nstr(abs(accurate.Mosh3(k, 1, 4,
    #    3)-accurate.Mosh3Slow(k, 1, 4,3,15)),4)) for k in accurate.plus])
    print([mp.nstr(abs(accurate.Mosh3(k, 1, 4,
        3)-accurate.Mosh3Slow(k, 1, 4,3,5)),2) for k in accurate.plus])
    print([mp.nstr(abs(accurate.Mosh3(k, 1, 4,
        3)-accurate.Mosh3Slow(k, 1, 4,3,5)),2) for k in accurate.minus])
    print([mp.nstr(abs(accurate.Mosh3(k, 1, 4,
        3)-accurate.Mosh3Slow(k, 1, 4,3,5)),2) for k in accurate.real])
    #print([(mp.nstr(abs(accurate.Mosh3(k,1,4,3)),4),mp.nstr(abs(accurate.Mosh3(k, 1, 4,
    #    3)-accurate.Mosh3Slow(k, 1, 4,3,15)),4)) for k in accurate.plus])
    print('''accuracy of Mosh3 at the first twenty roots in each direction along
            all three branches''')
    t=17
    print([mp.nstr(abs(accurate.Mosh3(k, 1, 4,
        t)-accurate.Mosh3Slow(k, 1, 4,t,500)),2) for k in accurate.plus])
    """

    """
    acc= HighPrecision(24)
    #acc.zeros(20,mpf(4))
    x = np.linspace(1e-5, 25, 750)
    y = [abs(sum(acc.ErfCancelCautiousMP(mp.exp(pii*3j/8)*k))
             - mp.erf(mp.exp(pii*3j/8)*k))/(abs(mp.erf(mp.exp(pii*3j/8)*k))) for k
             in -x[::-1]]+[abs(sum(acc.ErfCancelCautiousMP(mp.exp(pii*3j/8)*k))
                              - mp.erf(mp.exp(pii*3j/8)*k))/(abs(sum(acc.ErfCancelCautiousMP(mp.exp(pii*3j/8)*k))))
                              for k in x]
    z = [abs(sum(acc.ErfCancelCautiousMP(mp.exp(-pii*3j/8)*k))
             - mp.erf(mp.exp(-pii*3j/8)*k))/(abs(mp.erf(mp.exp(-pii*3j/8)*k))) for k
             in -x[::-1]]+[abs(sum(acc.ErfCancelCautiousMP(mp.exp(-pii*3j/8)*k))
                              - mp.erf(mp.exp(-pii*3j/8)*k))/(abs(mp.erf(-mp.exp(pii*3j/8)*k)))
                              for k in x]

    fig, ax = plt.subplots(2,1)

    # Using set_dashes() to modify dashing of an existing line
    ax[0].plot([-k for k in x[::-1]]+[k for k in x], y )
    ax[0].set_ylabel(r'erf error $\varphi=3\pi/8$')

    ax[1].plot([-k for k in x[::-1]]+[k for k in x], z )
    ax[1].set_ylabel(r'erf error $\varphi=-3\pi/8$')
    # Using plot(..., dashes=...) to set the dashing when creating a line
    #line2, = ax.plot(x, y - 0.2, dashes=[6, 2], label='Using the dashes parameter')

    fig.tight_layout()
    plt.show()
    """


    """
    plus,minus,reals=gauss_trio_zeros(20,4)
    #print([Mosh3(k,1,4,3) for k in plus])
    print([(abs(Mosh3(k,1,4,3)),abs(Mosh3(k, 1, 4, 3)-Mosh(k, 1, -1j*4+1j*3) -
           Mosh(k, 1, +1j*4+1j*3) - Mosh(k, 1, 4+1j*3))) for k in plus])
    print(abs(Mosh3( newton_gauss(-np.exp(3j*pi/8)
        * np.sqrt(pi*(2*18-1) / np.sqrt(2)/4), 4) ,1,4,3)))
    print(abs(Mosh3(newton_gauss(-np.exp(3j*pi/8)
        * np.sqrt(pi*(2*17-1) / np.sqrt(2)/4), 4),1,4,3)))
    """

"""
    acc= HighPrecision(42)
    acc.zeros(10,mpf(4))

    x = np.linspace(0, 7.5, 75)
    y = [np.log(sum([float(abs(accurate.Mosh3(k, 1, 4,
        xx)-accurate.Mosh3Slow(k, 1, 4,xx,15))) for k in accurate.plus])) for xx
        in x]

    fig, ax = plt.subplots(2,1)

    # Using set_dashes() to modify dashing of an existing line
    ax[0].plot(x, y )
    ax[0].set_ylabel(r'erf error $\varphi=3\pi/8$')

    # Using plot(..., dashes=...) to set the dashing when creating a line
    #line2, = ax.plot(x, y - 0.2, dashes=[6, 2], label='Using the dashes parameter')

    fig.tight_layout()
    plt.show()
"""
