# Tunneling
This repo contains two python files, Moshinski.py which contains an
implementation of a representation of the time dependent wave-function on the
half line subject to Eckart's potential based on resonances. There are two
versions: psi_standard, an implementation using double precision for most of the
calculation except for initialization and psi, an implementation using arbitrary
precision throughout. For high barriers high precision is necessary.
The second file is asymptotic_plot. This file contains a plot of the function
psi or psi_standard evaluated at a point as a function of time and compares this
with an approximation with exponential decay as was done since Gamow and with
the asymptotically late expression.
