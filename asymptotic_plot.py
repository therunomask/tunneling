import numpy as np
import matplotlib
matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
import Moshinski
from mpmath import mp,mpmathify,mpf,mpc,hyp2f1
import cProfile
from multiprocessing import Pool

rootrad=120
delta=7
de=delta
digits=60
#x = np.linspace(beg, end, 630)
#y1=[ps.eval(mpf(xx), mpf(1)) for xx in x]
"""
def foo():
    #y2=[ps.eval(mpf(xx), mpf(1)) for xx in x]
    y2=[ps.eval(xx, 1) for xx in x]

cProfile.run('foo()')
"""
#also plot exponential term of asymptotic series
def asymp_exp(ps,r,t):
    const=2*ps.k0*ps.Ck0
    f_s=ps.prec.jost(ps.de,-ps.k0,r,ps.rho)/\
            ps.prec.djost_dk(ps.de,-ps.k0,ps.rho)
    return complex(const*f_s*np.exp(complex(-1j*ps.k0*ps.k0*t)))

def plot_psi_exp_talg():
    ps=Moshinski.psi_standard(digits,rootrad,1.25,delta,1)
    beg=0.5
    end=30.
    x = np.linspace(beg, end, 230)
    r=0.5
    print(f"talg at r={r} is given by {ps.talg(r)}")
    y2=[np.abs(ps.eval(xx, r)) for xx in x]
    #y=[float(abs(y1[k]-y2[k])) for k in range(len(y1))]
    y = [float(np.log(abs(y2[xx]))) for xx in range(len(y2))]

    #make exp invisible after t=15
    z = [np.abs(asymp_exp(ps,r,xx)) for xx in x]
    logz= [np.log(x) for x in z]
    alg= [np.abs(complex(ps.psi_infty(r)/xx**(1.5))) for xx in x]
    log_alg=[np.log(x) for x in alg]

    fig, ax = plt.subplots(2,1)

    ax[0].plot(x, y ,color='blue')
    ax[0].plot(x,logz,color='green')
    ax[0].plot(x,log_alg,color='orange')
    ax[0].set_ylabel(r'decay of $\psi$ logarithmically')
    #y = [float(abs(y1[xx])) for xx in range(len(y1))]
    ax[1].plot(x, y2 ,color='blue')
    ax[1].plot(x,z,color='green')
    ax[1].plot(x,alg,color='orange')
    ax[1].set_ylabel(r'decay of $\psi$')
    fig.tight_layout()
    plt.savefig(f"r={r}_decay_de={delta}_rootrad={rootrad}_digits={digits}_from{beg}to{end}.png")
    plt.show()

def pretty_plot():
    #ps=Moshinski.psi_standard(digits,rootrad,1.25,delta,1)
    ps=Moshinski.psi_standard(digits,rootrad,1.25,delta,1)
    beg=0.05
    end=20.
    x = np.linspace(beg, end, 230)
    r=0.5
    print(f"talg at r={r} is given by {ps.talg(r)}")
    talg=ps.talg(r)
    y2=[np.abs(complex(ps.eval(xx, r))) for xx in x]
    #y=[float(abs(y1[k]-y2[k])) for k in range(len(y1))]
    y = [float(np.log(abs(y2[xx]))) for xx in range(len(y2))]

    #make exp invisible after t=15
    z = [np.abs(asymp_exp(ps,r,xx)) for xx in x]
    logz= [np.log(x) for x in z]
    alg= [np.abs(complex(ps.psi_infty(r)/xx**(1.5))) for xx in x]
    log_alg=[np.log(x) for x in alg]

    fig, ax = plt.subplots()

    ax.plot(x, y ,color='blue',linewidth=0.9,label=r'$\log(|\psi(r,t)|)$')
    expplot, =ax.plot(x,logz,color='darkgreen',linewidth=2.5,
            label=r'$\log(|2 k_0 C(k_0)f(-k_0,r) e^{-it k_0^2}/f(-k_0,0)|)$')
    expplot.set_dashes([4,3])
    asymplot, =ax.plot(x,log_alg,color='purple',linewidth=2.5,
            label=r'$\log(|\psi_\infty(r)|/t^{1.5})$')
    asymplot.set_dashes([0.7,1.4])
    ax.set_xlabel(r't')
    #intersect=np.array([np.log(np.abs(asymp_exp(ps,r,talg)))])
    plt.xlim([beg,end])
    yspan=np.max(y)-np.min(y)
    bot=np.min(y)-yspan*0.05
    top=np.max(y)+yspan*0.05
    plt.ylim([bot,top])
    markers, stems, base =ax.stem(np.array([np.real(talg)]),top,linefmt='C7-',markerfmt='',bottom=bot)
    stems.set_linewidth(0.6)
    fig.tight_layout()
    plt.legend()
    plt.savefig(f"r={r}_decay_de={delta}_rootrad={rootrad}_digits={digits}_from{beg}to{end}.png")
    plt.show()

def prec_abs(arg):
    ps, t,r=arg
    return np.abs(complex(ps.eval(t,r)))

def pretty_plot_prec():
    #ps=Moshinski.psi_standard(digits,rootrad,1.25,delta,1)
    ps=Moshinski.psi(digits,rootrad,mpmathify(1.25),mpmathify(delta),mpmathify(1))
    beg=0.05
    end=35.
    x = np.linspace(beg, end, 630)
    r=0.5
    print(f"talg at r={r} is given by {ps.talg(r)}")
    talg=ps.talg(r)

    with Pool(8) as p:
      y2=p.map(prec_abs,[[ps,xx, r] for xx in x])  
    #y2=[np.abs(complex(ps.eval(mpmathify(xx), mpmathify(r)))) for xx in x]
    #y=[float(abs(y1[k]-y2[k])) for k in range(len(y1))]
    y = [float(np.log(abs(y2[xx]))) for xx in range(len(y2))]

    #make exp invisible after t=15
    z = [np.abs(asymp_exp(ps,r,xx)) for xx in x]
    logz= [np.log(x) for x in z]
    alg= [np.abs(complex(ps.psi_infty(r)/xx**(1.5))) for xx in x]
    log_alg=[np.log(x) for x in alg]

    fig, ax = plt.subplots()

    ax.plot(x, y ,color='blue',linewidth=0.9,label=r'$\log(|\psi(r,t)|)$')
    expplot, =ax.plot(x,logz,color='darkgreen',linewidth=2.5,
            label=r'$\log(|2 k_0 C(k_0)f(-k_0,r) e^{-it k_0^2}/f(-k_0,0)|)$')
    expplot.set_dashes([4,3])
    asymplot, =ax.plot(x,log_alg,color='purple',linewidth=2.5,
            label=r'$\log(|\psi_\infty(r)|/t^{1.5})$')
    asymplot.set_dashes([0.7,1.4])
    ax.set_xlabel(r't')
    #intersect=np.array([np.log(np.abs(asymp_exp(ps,r,talg)))])
    plt.xlim([beg,end])
    mi=np.min([np.real(complex(u)) for u in y])
    ma=np.max([np.real(complex(u)) for u in y])
    yspan=ma-mi
    bot=mi-yspan*0.05
    top=ma+yspan*0.05
    plt.ylim([bot,top])
    markers, stems, base =ax.stem(np.array([np.real(complex(talg))]),top,linefmt='C7-',markerfmt='',bottom=bot)
    stems.set_linewidth(0.6)
    fig.tight_layout()
    plt.legend()
    plt.savefig(f"r={r}_decay_de={delta}_rootrad={rootrad}_digits={digits}_from{beg}to{end}.png",dpi=400)
    plt.show()

def plot_psi_by_alg():
    """ plot |\psi_infty/t^1.5/\psi| for very long times"""
    ps=Moshinski.psi_standard(digits,rootrad,1.25,delta,1)
    r=0.5
    t = [10**k for k in range(2,20)]
    y = [np.abs(complex(ps.psi_infty(r)/xx**(1.5))/ps.eval_corrected(xx, r)) for xx in t]
    print(f"""psi_infty={ps.psi_infty(r)},
            abs()={np.abs(complex(ps.psi_infty(r)))}""")
    print(y)
    fig, ax = plt.subplots(3,1)
    ax[0].plot([k for k in range(2,20)],y)
    ax[1].plot([k for k in range(9,20)],y[7:])
    ax[2].plot([k for k in range(16,20)],y[14:])
    plt.show()

def plot_psi_acc_by_alg():
    """ plot |\psi_infty/t^1.5/\psi| for very long times"""
    digits=16
    ps=Moshinski.psi(digits,rootrad,1.25,delta,1)
    r=0.5
    t = [10**k for k in range(2,20)]
    y = [np.abs(complex(ps.psi_infty(r)/xx**(1.5))/ps.eval(xx, r)) for xx in t]
    print(f"t_alg={ps.talg(r)}")
    print(f"\sum_n a_n/k_n^2={ps.aSum(r,1)}")
    print(f"\sum_n a_n/k_n^2={ps.aSum(r,2)}")
    z1=1/(1+np.exp(r-1))
    z2=1/(1+np.exp(-1))
    print(f"""-C(0)f(0,r)/(3
            f(0,0))={-ps.C0*ps.prec.wr(de,0,z1)/ps.prec.wr(de,0,z2)/3}""")
    print(f"\sum_n a_n/k_n^3={ps.aSum(r,3)}")
    z1=1/(1+np.exp(r-1))
    z2=1/(1+np.exp(-1))
    fr=ps.prec.jost(de,0,r,1)
    f=ps.prec.jost(de,0,0,1)
    dfr=ps.prec.djost_dk_gen_r(de,0,r,1)
    df=ps.prec.djost_dk_gen_r(de,0,0,1)
    der=-1j*r*fr/f-dfr/f+fr*df/f/f
    print(f"""-C(0)\partial_k e^(-ikr)f(-k,r)/(3
            f(-k,0))_|(k=0)={-ps.C0*der/3}""")
    print(f"""psi_infty={ps.psi_infty(r)},
            abs()={np.abs(complex(ps.psi_infty(r)))}""")
    print(y)
    fig, ax = plt.subplots(3,1)
    ax[0].plot([k for k in range(2,20)],y)
    ax[1].plot([k for k in range(9,20)],y[7:])
    ax[2].plot([k for k in range(16,20)],y[14:])
    plt.show()

def prec(arg):
    ps, t,r,end=arg
    print("currently at ",r," of ",end)
    return complex(ps.eval(t,r))

def output_values():
    ps=Moshinski.psi(digits,rootrad,mpmathify(1.25),mpmathify(delta),mpmathify(1))
    beg=900.
    end=1350.
    dx=0.15625
    x = np.linspace(beg, end,int( (end-beg)/dx))
    t=100.0
    y=[]
    with Pool(8) as p:
      y=p.map(prec,[[ps,t, r,end] for r in x])  

    f = open(f"ψ_t={t}_r_in{beg}to{end}_for_de_{de}_rootrad_{rootrad}.txt","w")
    for yy in y:
        print(yy,file=f)
    f.close()

'''
    t=30.0
    y=[]
    with Pool(8) as p:
      y=p.map(prec,[[ps,t, r,end] for r in x])  

    f = open(f"ψ_t={t}_r_in{beg}to{end}_for_de_{de}_rootrad_{rootrad}.txt","w")
    for yy in y:
        print(yy,file=f)
    f.close()
'''

#pretty_plot_prec()


output_values()
