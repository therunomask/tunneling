from mpmath import mp, mpc, mpf, hyp2f1
import numpy as np

def wr(de,k,z):
    return hyp2f1(mpc(1)/2-1j*mpc(de),mpc(1)/2+1j*mpc(de),1+2j*k,z)

def wArray(de,k,z):
    r=np.zeros(np.shape(k))
    for i,v in np.ndenumerate(k):
        r[i]=float(abs(hyp2f1(1/2-1j*de,1/2+1j*de,1-2j*v,z)))
    return r

def hypder(de,k,z):
    h=hyp2f1
    #adjust accuracy of calculation
    #adjustment based on assumption that values of f are of order 1
    old=mp.dps
    mp.dps=mp.ceil((5*mp.log(5)/4-mp.log(4))/mp.log(10)+5*mp.dps/4)
    d=mp.exp(-mp.dps/5*mp.log(10)-mp.log(4)/5)
    r=-wr(de,k+2*d,z)/12+2*wr(de,k+d,z)/3-wr(de,k-d,z)*2/3+wr(de,k-2*d,z)/12
    r/=d
    mp.dps=old
    return r
    
def NewtonHypder(de,k,z):
    c=0
    f=wr(de,k,z)
    while c<10 and mp.fabs(f)>1e-14:
        print(f"x={k}, |f(x)|={abs(f)}")
        k=k-f/hypder(de,k,z)
        f=wr(de,k,z)
        c+=1
    if mp.fabs(f)>1e-14:
        print(f"hard to improve on |f|={mp.fabs(f)}")
    return k
#print(NewtonHypder(1,-0.5+0.6j,mp.log(2)))

def jost(de,k,r,rho):
    z=1/(mpc(1)+mp.exp(r-rho))
    return mp.exp(-1j*k*r)*wr(de,k,z)

def djost_dk(de,k,r,rho):
    z=1/(mpc(1)+mp.exp(r-rho))
    r1=-1j*r*mp.exp(-1j*r*k)*wr(de,r,z)
    r2=mp.exp(-1j*k*r)*hypder(de,k,z)
    return r1+r2

def psi0(r,rho):
    return r*2*(2/rho)**(3/2)/mp.pi**(1/4) *mp.exp(-2*(r/rho)**2)

print(f"jost\': {djost_dk(1,1,1,1)}")

k=30*(mp.cos(3/8*mp.pi)+mp.sin(3/8 * mp.pi)*1j)
mp.dps=20
de=mpc(0.25)
rho=mpf(1)
print("dps: 20, standard degree")
from datetime import datetime
mytimestamp = []
mytimestamp.append(datetime.now())
print((mp.quad(lambda x:
    jost(de,-k,x,rho)*psi0(x,rho),[0,mp.inf])*jost(de,k,0,rho)-
    jost(de,-k,0,rho)*mp.quad(lambda x:
        jost(de,k,x,rho)*psi0(x,rho),[0,mp.inf]))/(2j*k))
mytimestamp.append(datetime.now())
print((mp.quad(lambda x:
    djost_dk(de,-k,x,rho)*psi0(x,rho),[0,mp.inf])*jost(de,k,0,rho)-
    jost(de,-k,0,rho)*mp.quad(lambda x:
        djost_dk(de,k,x,rho)*psi0(x,rho),[0,mp.inf]))/(2j*k))
mytimestamp.append(datetime.now())
print("dps=20, degree=12")
print((mp.quad(lambda x:
    jost(de,-k,x,rho)*psi0(x,rho),[0,mp.inf],maxdegree=12)*jost(de,k,0,rho)-
    jost(de,-k,0,rho)*mp.quad(lambda x:
        jost(de,k,x,rho)*psi0(x,rho),[0,mp.inf],maxdegree=12))/(2j*k))
mytimestamp.append(datetime.now())
print((mp.quad(lambda x:
    djost_dk(de,-k,x,rho)*psi0(x,rho),[0,mp.inf],maxdegree=12)*jost(de,k,0,rho)-
    jost(de,-k,0,rho)*mp.quad(lambda x:
        djost_dk(de,k,x,rho)*psi0(x,rho),[0,mp.inf],maxdegree=12))/(2j*k))
mytimestamp.append(datetime.now())
mp.dps=30
print((mp.quad(lambda x:
    jost(de,-k,x,rho)*psi0(x,rho),[0,mp.inf],maxdegree=12)*jost(de,k,0,rho)-
    jost(de,-k,0,rho)*mp.quad(lambda x:
        jost(de,k,x,rho)*psi0(x,rho),[0,mp.inf],maxdegree=12))/(2j*k))
mytimestamp.append(datetime.now())
mp.dps=40
print((mp.quad(lambda x:
    jost(de,-k,x,rho)*psi0(x,rho),[0,mp.inf],maxdegree=12)*jost(de,k,0,rho)-
    jost(de,-k,0,rho)*mp.quad(lambda x:
        jost(de,k,x,rho)*psi0(x,rho),[0,mp.inf],maxdegree=12))/(2j*k))
mytimestamp.append(datetime.now())
print((mp.quad(lambda x:
    jost(de,-k,x,rho)*psi0(x,rho),[0,mp.inf],maxdegree=14)*jost(de,k,0,rho)-
    jost(de,-k,0,rho)*mp.quad(lambda x:
        jost(de,k,x,rho)*psi0(x,rho),[0,mp.inf],maxdegree=14))/(2j*k))
mytimestamp.append(datetime.now())
print((mp.quadosc(lambda x:
    jost(de,-k,x,rho)*psi0(x,rho),[0,mp.inf],omega=mp.re(k))*jost(de,k,0,rho)-
    jost(de,-k,0,rho)*mp.quad(lambda x:
        jost(de,k,x,rho)*psi0(x,rho),[0,mp.inf],omega=mp.re(k)))/(2j*k))
mytimestamp.append(datetime.now())
i = 1
while i < len(mytimestamp):
    print("{}th print: {:.2f}".format((i-1),
        (mytimestamp[i]-mytimestamp[i-1]).total_seconds()))
    i+=1

"""
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt

delta = 0.025
x = np.arange(0.1, 3.0, delta)
y = np.arange(-.75, 0, delta)
X, Y = np.meshgrid(x, y)
#Z1 = np.exp(-X**2 - Y**2)
#Z2 = np.exp(-(X - 1)**2 - (Y - 1)**2)
#Z = (Z1 - Z2) * 2
Z=wArray(0.25,X+1j*Y,0.75)
fig, ax = plt.subplots()
CS = ax.contour(X, Y, Z,20)
ax.clabel(CS, inline=True, fontsize=10)
ax.set_title('Simplest default with labels')
plt.show()"""
